import 'package:flutter/material.dart';
import 'package:lecture_practice5/page.dart';
import 'package:lecture_practice5/provider.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => MyProvider(),
      child: MaterialApp(
        title: 'Practice5',
        theme: ThemeData.dark(),
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          'Page One',
        ),
      ),
      body: Column(
        children: [
          const SizedBox(height: 258.0),
          Center(
            child: Column(
              children: [
                Consumer<MyProvider>(
                  builder: (context, value, _) {
                    return Text(
                      '${value.number}',
                      style: TextStyle(
                        fontSize: 28.0,
                      ),
                    );
                  },
                ),
                const SizedBox(height: 30.0),
                SizedBox(
                  width: 98.0,
                  height: 48.0,
                  child: Material(
                    borderRadius: BorderRadius.circular(18.0),
                    color: Colors.green[400],
                    child: InkWell(
                      borderRadius: BorderRadius.circular(18.0),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => PageTwo(),
                          ),
                        );
                      },
                      child: Center(
                        child: Text(
                          'next page',
                          style: TextStyle(color: Colors.black45, fontSize: 16),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
