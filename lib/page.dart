import 'package:flutter/material.dart';
import 'package:lecture_practice5/button.dart';

class PageTwo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Page Two'),
      ),
      body: Column(
        children: [
          Expanded(
            child: Container(),
          ),
          Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Decrement(),

                const SizedBox(width: 100),

                Increment(),
              ],
            ),
          ),
          Expanded(
            child: Container(),
          ),
        ],
      ),
    );
  }
}
