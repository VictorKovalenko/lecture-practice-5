import 'package:flutter/material.dart';
import 'package:lecture_practice5/provider.dart';
import 'package:provider/provider.dart';



class Increment extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return  Consumer<MyProvider>(
      builder: (context, value, _) {
        return Container(
          height: 48.0,
          width: 98.0,
          child: Material(
            borderRadius: BorderRadius.circular(18.0),
            color: Colors.green[400],
            child: InkWell(
              borderRadius: BorderRadius.circular(18.0),
              onTap: value.increment,
              child: Center(
                child: Text('+',style: TextStyle(color: Colors.black45,fontSize: 18.0),),
              ),
            ),
          ),
        );
      },
    );
  }
}


class Decrement extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return  Consumer<MyProvider>(
      builder: (context, value, _) {
        return Container(
          height: 48.0,
          width: 98.0,
          child: Material(
            borderRadius: BorderRadius.circular(18.0),
            color: Colors.green[400],
            child: InkWell(
              borderRadius: BorderRadius.circular(18.0),
              onTap: value.decrement,
              child: Center(
                child: Text('-',style: TextStyle(color: Colors.black45,fontSize: 18.0),),
              ),
            ),
          ),
        );
      },
    );
  }
}